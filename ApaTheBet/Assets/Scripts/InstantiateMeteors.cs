﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateMeteors : MonoBehaviour
{
    public GameObject meteorPrefab;
    public float respawnTime = 2f;
    Vector2 startSpawn, screenBounds;

    GameObject[] Meteors;
     int arrS = 15;

    void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        SendMessage("SpawnMeteors");
    }

    void SpawnMeteors()
    {
        Meteors = new GameObject[arrS];
        for (int i = 0; i < arrS; i++)
        {
            startSpawn = new Vector2(Random.Range(-screenBounds.x, screenBounds.x), screenBounds.y * Random.Range(0.4f, 1));
            Meteors[i] = Instantiate(meteorPrefab, startSpawn, Quaternion.identity);//Instantiate meteors
        }

    }

    private void Update()
    {
        if (PlayerController.getDead()) 
        {
            Destroy(this.gameObject);
        }
    }

    //IEnumerator meteorsRespawn()
    //{
    //    while (true)//TODO while !tag == End, if end - screen freeze, show score, again
    //    {
    //        respawnTime = Random.Range(2f, 5f);
    //        yield return new WaitForSeconds(respawnTime);
    //        SpawnMeteors();

    //    }
    //}
}

//    private int arrSize = 5;
//    [SerializeField]
//    private GameObject[] MeteorsArr;
//    public GameObject meteorPrefab;

//    //[SerializeField] [Range(0f, 4f)] float LerpTime;
//    //int posLerp = 0;
//    //int distLerp;
//    //float t;

//    public Vector2 startMarker = new Vector2(-2, 5);
//    public Vector2 endMarker;

//    // Movement speed in units per second.
//    public float speed = 1.0F;

//    // Time when the movement started.
//    private float startTime;

//    // Total distance between the markers.
//    private float journeyLength;




//    float x_Pos = 0;
//    float yPos = 0;

//  //  private Vector2 meteorPos = new Vector2(-2, 5);//initial position to spawn from

//    public float respawnRate = 50f;//defines when it's time to respawn
//    public float distanceBeforeRespw = 0f;

//    public float min = -5f;//borders of random spawn
//    public float max = 5f;


//    private int meteorRespawn = 0;//how many meteors are already spawned


//    void Start()
//    {
//        MeteorsArr = new GameObject[arrSize];
//        for (int i = 0; i < arrSize; i++)
//        {
//            MeteorsArr[i] = Instantiate(meteorPrefab, startMarker, Quaternion.identity);//Instantiate meteors
//        }
//        // t = distanceBeforeRespw / respawnRate;

//        // Keep a note of the time the movement started.
//        startTime = Time.time;

//        // Calculate the journey length.
//        journeyLength = Vector2.Distance(startMarker, endMarker);
//    }

//    // Update is called once per frame
//    void Update()
//    {


//        ////  timerBeforeRespawn += Time.deltaTime;//update timer
//        //if (MeteorsArr[meteorRespawn].transform.position.y > -5f)
//        //{
//        //    yPos = 0;//set a new timer
//        //  //  Debug.Log("BeforeResp" + distanceBeforeRespw);
//        //   // Debug.Log("\nTime Paseed" + Time.time);
//        //    x_Pos = Random.Range(min, max);//set position      

//        //}
//        //else
//        //{
//            for (int i = 0; i < arrSize; i++)
//            {
//                //x_Pos +=2;//set position      
//                //yPos = 0;
//                //t = +t * t * (3f - 2f * t) * Time.deltaTime;


//                // Distance moved equals elapsed time times speed..
//                float distCovered = (Time.time - startTime) * speed;

//                // Fraction of journey completed equals current distance divided by total distance.
//                float fractionOfJourney = distCovered / journeyLength;

//                // Set our position as a fraction of the distance between the markers.
//                MeteorsArr[i].transform.position = Vector2.Lerp(startMarker, endMarker, fractionOfJourney);

//              //  MeteorsArr[i].transform.position = new Vector3(x_Pos, yPos+3); //TODO
//                //meteorRespawn++;
//            }
//            //if (meteorRespawn > arrSize - 1)//...and start again
//            //{
//            //    meteorRespawn = 0;
//            //}



//        //}

//    }


//}
