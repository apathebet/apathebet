﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorsMovement : MonoBehaviour//this script creates a simple linear movement to random points
{

    float speed, acceleration = 0.2f;

    float fraction = 0.2f;
    float startTimer = .8f;

    Vector2 targetPosition;

    public AnimationCurve animationCurve;

    // Use this for initialization
    void Start()
    {
        // select a new random target position
        RandTargetPoints();
        speed = 0;
        SpeedAndTimerReset();

    }

    void SpeedAndTimerReset()
    {
        startTimer = Time.time;
        speed = Random.Range(1, 5);
    }


    

    // Update is called once per frame
    void Update()
    {
        // the percent of time has passed to the speed change interval
        float percent = (Time.time - startTimer) / fraction;

        if (percent > 1)
        {           
            percent = 0;
            SpeedAndTimerReset();
        }

        // calculate the current speed, using percent and the animation curve
         speed = Mathf.Lerp(Random.Range(1, 3), Random.Range(2, 7), animationCurve.Evaluate(percent));


        speed += Time.deltaTime * acceleration;
        // move toward the target position using the interpolated speed
        transform.position = Vector2.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);

        // if we've reached our destination, set a new destination
        if (Vector3.Distance(transform.position, targetPosition) < .2f)
        {
            RandTargetPoints();
        }
    }


    void RandTargetPoints()
    {
        targetPosition = Camera.main.ScreenToWorldPoint(new Vector2(Random.Range(1, Screen.width*0.9f), Random.Range(1, Screen.height * 0.9f)));
    }

    //this script has problem with bounds of moving objects
    /*    float RepeateRate = 4f;
        Vector2[] Points;
        int arrS = 5;

       Rigidbody2D rb;


       void Start()
       {
           rb = this.GetComponent<Rigidbody2D>();

           Points = new Vector2[arrS];//random points
               for(int i = 0; i < arrS; i++)
               {
               // Points[i] = new Vector2(Random.Range(-8, 8), Random.Range(-5, 5));
               Points[i] = new Vector2(Random.Range(-2,2), Random.Range(-3, 2));
           }
          StartCoroutine(LerpEInvoke());
       }



       IEnumerator LerpEInvoke()
       {

           while (!PlayerController.getDead())//while Player is not dead
          {
               int index = Random.Range(0, Points.Length - 1);//select point
               yield return StartCoroutine(LerpToPoint(Points[index]));// interpolate the object to the point

               yield return new WaitForSeconds(RepeateRate);
           }

       }



       IEnumerator LerpToPoint(Vector2 endPoint)
       {
           var fraction = 0.2f;
           var startPoint = transform.position;//initial position
           float journeyLength = Vector2.Distance(startPoint, endPoint);

            float speed = Random.Range(1, 5);


           while (fraction < 3f)//interpolate within 3, choose another point
           {
               Vector2 sp = new Vector2(0, 2f);
               Vector2 spE = new Vector2(0, -5f);

               fraction += Time.deltaTime / journeyLength;
               Debug.Log(fraction);
               rb.velocity = Vector2.Lerp(startPoint, endPoint, fraction);
               // rb.transform.position = Vector2.Lerp(startPoint, endPoint, fraction * Time.deltaTime);
        transform.position = Vector2.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);

           }

           yield return null;
       }*/
}