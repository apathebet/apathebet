﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
     static bool isDead = false;
      Rigidbody2D rb;
     public float speed = 1.5f;
     Animator animatorPl;
     Vector3 targetPosition;

    static public bool getDead() { return isDead; }
    static public void setDead(bool isD) { isDead = isD; }



     void Awake()
     {
         rb = GetComponent<Rigidbody2D>();
         animatorPl = GetComponent<Animator>();

         targetPosition = transform.position;

     }

     void Update()
     {
         if (!PlayerController.getDead())
         Move();

     }

     void Move()
     {
         if (Input.GetMouseButtonDown(0))
         {
             targetPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
             targetPosition.z = transform.position.z;
         }
         float fraction = Time.deltaTime / speed;
         rb.transform.position = Vector3.MoveTowards(this.transform.position, targetPosition, speed * Time.deltaTime);

     }





   /* public float minSpeed;
    public float maxSpeed;

    public float speed;

    public float changeSpeedInterval;
    float startTimer;

    float initialSpeed;
    float targetSpeed;

    Vector2 targetPosition;

    public AnimationCurve animationCurve;

    // Use this for initialization
    void Start()
    {
        // select a new random target position
        ChooseNewRandomTargetPosition();

        // reset the current speed and initial speed, in case they've been changed in the Inspector
        initialSpeed = 0;
        speed = 0;
        // reset the timer
        ResetTimer();
        // choose a new target speed
        SetNewTargetSpeed();
    }

    void ResetTimer()
    {
        startTimer = Time.time;
    }

    void SetNewTargetSpeed()
    {
        // the new initial speed is the old target speed
        initialSpeed = targetSpeed;
        // set a new target speed
        targetSpeed = Random.Range(minSpeed, maxSpeed);
    }

    void ChooseNewRandomTargetPosition()
    {
        targetPosition = Camera.main.ScreenToWorldPoint(new Vector2(Random.Range(0, Screen.width), Random.Range(0, Screen.height)));
    }

    // Update is called once per frame
    void Update()
    {
        // the time elapsed since the timer was started
        float timeElapsed = Time.time - startTimer;
        // the percent of time elapsed in relation to the speed change interval
        float percent = timeElapsed / changeSpeedInterval;
        // if time has elapsed, set a new target speed

        if (percent > 1)
        {
            SetNewTargetSpeed();
            percent = 0;
            ResetTimer();
        }

        // calculate the current speed, using percent and the animation curve
        speed = Mathf.Lerp(initialSpeed, targetSpeed, animationCurve.Evaluate(percent));

        // move toward the target position using the interpolated speed
        transform.position = Vector2.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);

        // if we've reached our destination, set a new destination
        if (Vector3.Distance(transform.position, targetPosition) < .1f)
        {
            ChooseNewRandomTargetPosition();
        }
    } 
   */
}
